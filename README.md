# bbhooks

bbhooks is bitbucket webhook receiver

post hook response detail page: [POST hook management](https://confluence.atlassian.com/display/BITBUCKET/POST+hook+management)

# Useage


```go
package main

import (
	"fmt"

	"bitbucket.org/Konboi/bbhooks"
)

func main() {
	port := 2299
	hooks := bbhooks.NewServer(port)

	hooks.On(someHandler)
	hooks.Run()
}

func someHandler(payload interface{}) {
	fmt.Printf("#v", payload)
}
```
