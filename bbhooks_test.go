package bbhooks

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

var count int

func SomeEvent(payload interface{}) {
	count++
}

func TestEmmit(t *testing.T) {
	hooks := NewServer(99999)
	hooks.On(SomeEvent)

	var payload interface{}
	Emmit(payload)

	if count != 1 {
		t.Fatal("Not Call SomeEvent")
	}
}

func TestReciver(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	w := httptest.NewRecorder()
	Reciver(w, req)
	if w.Code == 200 {
		t.Fatalf("Allowd only POST Method but expected status 200; received %d", w.Code)
	}

	json_string := `{"fuga": "hoge", "foo": { "bar": "boo" }}`
	req, _ = http.NewRequest("POST", "/", strings.NewReader("payload="+json_string))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	w = httptest.NewRecorder()
	Reciver(w, req)
	if w.Code != 200 {
		t.Fatalf("Not return 200; received %d", w.Code)
	}
}
