package bbhooks

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"strings"
)

const (
	VERSION = 0.1
)

type Server struct {
	Port int
}

type Hook struct {
	Func func(payload interface{})
}

type Hooks struct {
	Hooks []Hook
}

var hooks Hooks

func (s *Server) On(handler func(payload interface{})) {
	hooks.Hooks = append(hooks.Hooks, Hook{Func: handler})
}

func Emmit(payload interface{}) {
	for _, v := range hooks.Hooks {
		v.Func(payload)
	}
}

func NewServer(port int) *Server {
	return &Server{port}
}

func (s *Server) Run() error {
	log.Printf("ghooks server start 0.0.0.0:%d \n", s.Port)
	http.HandleFunc("/", Reciver)
	return http.ListenAndServe(":"+strconv.Itoa(s.Port), nil)
}

func Reciver(w http.ResponseWriter, req *http.Request) {
	if req.Method == "GET" {
		http.Error(w, "Method Not Allowd", http.StatusMethodNotAllowed)
		return
	}

	if req.Body == nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	defer req.Body.Close()

	var payload interface{}
	p := req.FormValue("payload")
	decoder := json.NewDecoder(strings.NewReader(p))

	err := decoder.Decode(&payload)
	if err != nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	Emmit(payload)
	w.WriteHeader(http.StatusOK)
}
